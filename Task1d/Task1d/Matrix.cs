﻿using System;
using System.Text;

namespace Task1d
{
    public class Matrix
    {
        private double[,] _matrix;
        private int _n;
        private int _m;

        public Matrix(int m, int n)
        {
            _matrix = new double[m, n];
            _m = m;
            _n = n;
        }

        public Matrix(double[,] matrix)
        {
            _matrix = matrix;
            _m = matrix.GetLength(0);
            _n = matrix.GetLength(1);
        }

        public double this[int i, int j]
        {
            get { return _matrix[i, j]; }

            set { _matrix[i, j] = value; }
        }

        public static Matrix Add(Matrix a, Matrix b)
        {
            return PerformOperation(a, b, (x, y) => x + y);
        }

        public static Matrix Substract(Matrix a, Matrix b)
        {
            return PerformOperation(a, b, (x, y) => x - y);
        }

        public static Matrix Multiply(Matrix a, Matrix b)
        {
            if (a._n != b._m && a._m != b._n)
            {
                throw new InvalidOperationException("Multiplication could not be performed to matrices with different dimensions");
            }

            var resultMatrix = new Matrix(a._m, b._n);
            for (var i = 0; i < a._m; i++)
            {
                for (var j = 0; j < b._n; j++)
                {
                    for (var k = 0; k < a._n; k++)
                    {
                        resultMatrix[i, j] += a[i, k] * b[k, j];
                    }
                }
            }

            return resultMatrix;
        }

        public override string ToString()
        {
            var resultString = new StringBuilder();
            for (var i = 0; i < _m; i++)
            {
                for (var j = 0; j < _n; j++)
                {
                    resultString.Append(_matrix[i, j] + " ");
                }

                resultString.Append(Environment.NewLine);
            }

            return resultString.ToString();
        }

        public double CalculateDeterminant()
        {
            if (_n != _m)
            {
                throw new InvalidOperationException("Determinant could not be calculated for not square matrix");
            }

            var leftTriangularMatrix = new Matrix(_n, _n);
            var rightTriangularMatrix = new Matrix(_n, _n);
            for (var i = 0; i < _n; i++)
            {
                rightTriangularMatrix[i, i] = 1;
            }

            for (var k = 0; k < _n; k++)
            {
                for (var i = 0; i < _n; i++)
                {
                    leftTriangularMatrix[i, k] = _matrix[i, k];
                    for (var p = 0; p < k; p++)
                    {
                        leftTriangularMatrix[i, k] -= leftTriangularMatrix[i, p] * rightTriangularMatrix[p, k];
                    }
                }

                for (var j = k + 1; j < _n; j++)
                {
                    rightTriangularMatrix[k, j] = _matrix[k, j];
                    for (var p = 0; p < k; p++)
                    {
                        rightTriangularMatrix[k, j] -= leftTriangularMatrix[k, p] * rightTriangularMatrix[p, j];
                    }

                    rightTriangularMatrix[k, j] /= leftTriangularMatrix[k, k];
                }
            }

            return DeterminantOfTriangularMatrix(leftTriangularMatrix);
        }

        public double FindFirstMinorOfOrderK(int k)
        {
            if (k <= 0 || k > _m || k > _n)
            {
                throw new ArgumentException("Minor could not be found for this order");
            }

            var minorMatrix = new Matrix(k, k);
            k -= 1;
            for (var i = 0; i <= k; i++)
            {
                for (var j = 0; j <= k; j++)
                {
                    minorMatrix[i, j] = _matrix[i, j];
                }
            }

            return minorMatrix.CalculateDeterminant();
        }

        public double FindComplementMinorOfOrderK(int k)
        {
            if (k <= 0 || k >= _m || k >= _n)
            {
                throw new ArgumentException("Minor could not be found for this order");
            }

            if (_n != _m)
            {
                throw new InvalidOperationException("Determinant could not be calculated for not square matrix");
            }

            var minorMatrix = new Matrix(_n - k, _n - k);
            k -= 1;
            for (var i = k + 1; i < _n; i++)
            {
                for (var j = k + 1; j < _n; j++)
                {
                    minorMatrix[i - k - 1, j - k - 1] = _matrix[i, j];
                }
            }

            return minorMatrix.CalculateDeterminant();
        }

        public double FindMinorij(int rowIndex, int columnIndex)
        {
            rowIndex -= 1;
            columnIndex -= 1;
            if (rowIndex < 0 || rowIndex >= _m || columnIndex < 0 || columnIndex >= _n)
            {
                throw new IndexOutOfRangeException("Indeces are out of range");
            }

            if (_n != _m)
            {
                throw new InvalidOperationException("Determinant could not be calculated for not square matrix");
            }

            var matrixWithoutRowAndColumn = new Matrix(_m - 1, _n - 1);
            for (var i = 0; i < rowIndex; i++)
            {
                for (var j = 0; j < columnIndex; j++)
                {
                    matrixWithoutRowAndColumn[i, j] = _matrix[i, j];
                }
            }

            for (var i = 0; i < rowIndex; i++)
            {
                for (var j = columnIndex + 1; j < _n; j++)
                {
                    matrixWithoutRowAndColumn[i, j - 1] = _matrix[i, j];
                }
            }

            for (var i = rowIndex + 1; i < _m; i++)
            {
                for (var j = 0; j < columnIndex; j++)
                {
                    matrixWithoutRowAndColumn[i - 1, j] = _matrix[i, j];
                }
            }

            for (var i = rowIndex + 1; i < _m; i++)
            {
                for (var j = columnIndex + 1; j < _n; j++)
                {
                    matrixWithoutRowAndColumn[i - 1, j - 1] = _matrix[i, j];
                }
            }

            return matrixWithoutRowAndColumn.CalculateDeterminant();
        }

        private static Matrix PerformOperation(Matrix a, Matrix b, Func<double, double, double> operation)
        {
            if (a._n != b._n || a._m != b._m)
            {
                throw new InvalidOperationException("Operation could not be performed to matrices with different dimensions");
            }

            var resultMatrix = new Matrix(a._m, b._n);
            for (var i = 0; i < a._m; i++)
            {
                for (var j = 0; j < a._n; j++)
                {
                    resultMatrix[i, j] = operation(a[i, j], b[i, j]);
                }
            }

            return resultMatrix;
        }

        private static double DeterminantOfTriangularMatrix(Matrix matrix)
        {
            double determinant = 1;
            for (var i = 0; i < matrix._n; i++)
            {
                determinant *= matrix[i, i];
            }

            return determinant;
        }
    }
}
