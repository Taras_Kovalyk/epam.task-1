﻿using System;

namespace Task1d
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                DemostrateWorkOfProgram();
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void DemostrateWorkOfProgram()
        {
            Console.WriteLine("Demonstrating work of program");
            Console.WriteLine("*******************************");
            Console.WriteLine("Creating new empty 4*5 matrix");
            var emptyMatrix = new Matrix(4, 5);
            Console.WriteLine(emptyMatrix);

            Console.WriteLine("Creating first matrix");
            var firstMatrix = new Matrix(new double[,] { { 1, 2, 3, 4 }, { 2, 5, 8, 7 }, { 3, 8, 15, 14 }, { 4, 7, 14, 27 } });
            Console.WriteLine(firstMatrix);

            Console.WriteLine("Creating second matrix");
            var secondMatrix = new Matrix(new double[,] { { 4, 1, 5, 4 }, { 7, 2, 6, 11 }, { 10, 4, 4, 13 }, { 9, 4, 7, 15 } });
            Console.WriteLine(secondMatrix);

            Console.WriteLine("Their sum:");
            Console.WriteLine(Matrix.Add(firstMatrix, secondMatrix));
            Console.WriteLine("Their difference");
            Console.WriteLine(Matrix.Substract(firstMatrix, secondMatrix));
            Console.WriteLine("Their product");
            Console.WriteLine(Matrix.Multiply(firstMatrix, secondMatrix));

            Console.WriteLine("Product of nonsquare matrices");
            var thirdMatrix = new Matrix(new double[,] { { 1, 2, 3 }, { 4, 5, 6 } });
            var fourthMatrix = new Matrix(new double[,] { { 1, 2 }, { 3, 4 }, { 5, 6 } });
            Console.WriteLine("Third matrix:");
            Console.WriteLine(thirdMatrix);
            Console.WriteLine("Fourth matrix");
            Console.WriteLine(fourthMatrix);
            Console.WriteLine("Product:");
            Console.WriteLine(Matrix.Multiply(thirdMatrix, fourthMatrix));

            Console.WriteLine("Trying to multiplicate matrices that can`t be multiplicated");
            try
            {
                Console.WriteLine(Matrix.Multiply(firstMatrix, thirdMatrix));
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message + Environment.NewLine);
            }

            Console.WriteLine($"Determinant of first matrix: {firstMatrix.CalculateDeterminant()}");
            Console.WriteLine($"Determinant of second matrix: {secondMatrix.CalculateDeterminant()}" + Environment.NewLine);

            try
            {
                Console.WriteLine("Trying to find determinant of third matrix");
                Console.WriteLine(thirdMatrix.CalculateDeterminant());
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message + Environment.NewLine);
            }

            Console.WriteLine($"(1,2)-Minor of first matrix: {firstMatrix.FindMinorij(1, 2)}" + Environment.NewLine);
            try
            {
                Console.WriteLine("Trying to find (5,2)-Minor of second matrix:");
                Console.WriteLine(secondMatrix.FindMinorij(5, 2));
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine(e.Message + Environment.NewLine);
            }

            Console.WriteLine($"Minor of k = 3 order of first matrix: {firstMatrix.FindFirstMinorOfOrderK(3)}" + Environment.NewLine);
            try
            {
                Console.WriteLine("Trying to find minor of k = 10 order of first matrix");
                Console.WriteLine(firstMatrix.FindFirstMinorOfOrderK(10));
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message + Environment.NewLine);
            }

            Console.WriteLine($"Complement minor of order k = 2 of first matrix {firstMatrix.FindComplementMinorOfOrderK(2)}" + Environment.NewLine);
            try
            {
                Console.WriteLine("Trying to find complement minor of order k = 4 of first matrix");
                Console.WriteLine(firstMatrix.FindComplementMinorOfOrderK(4));
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
