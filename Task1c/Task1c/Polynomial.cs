﻿using System;
using System.Text;

namespace Task1c
{
    public class Polynomial
    {
        private int _power;
        private double[] _coefficients;

        public Polynomial(int power, double[] coefficients)
        {
            if (power > coefficients.Length - 1)
            {
                _coefficients = coefficients;
                _power = _coefficients.Length - 1;
            }
            else if (power < coefficients.Length - 1)
            {
                _power = power;
                _coefficients = new double[power + 1];
                Array.Copy(coefficients, _coefficients, power + 1);
            }
            else
            {
                _power = power;
                _coefficients = coefficients;
            }
        }

        public int Power
        {
            get { return _power; }
            set { _power = value; }
        }

        public double[] Coefficients
        {
            get { return _coefficients; }
            set { _coefficients = value; }
        }

        public static Polynomial Add(Polynomial firstPolynomial, Polynomial secondPolynomial)
        {
            if (firstPolynomial._power > secondPolynomial._power)
            {
                return PerformOperation(firstPolynomial, secondPolynomial, (x, y) => x + y);
            }

            return PerformOperation(secondPolynomial, firstPolynomial, (x, y) => x + y);
        }

        public static Polynomial Substract(Polynomial firstPolynomial, Polynomial secondPolynomial)
        {
            if (firstPolynomial._power > secondPolynomial._power)
            {
                return PerformOperation(firstPolynomial, secondPolynomial, (x, y) => x - y);
            }

            return PerformOperation(ChangeSignOfCoefficients(secondPolynomial), firstPolynomial, (x, y) => x + y);
        }

        public static Polynomial Multiply(Polynomial firstPolynomial, Polynomial secondPolynomial)
        {
            var resultArrayOfCoefficients = new double[firstPolynomial._power + secondPolynomial._power + 1];
            for (var i = 0; i <= firstPolynomial._power; i++)
            {
                for (var j = 0; j <= secondPolynomial._power; j++)
                {
                    resultArrayOfCoefficients[i + j] += firstPolynomial._coefficients[i] * secondPolynomial._coefficients[j];
                }
            }

            return new Polynomial(firstPolynomial._power + secondPolynomial._power, resultArrayOfCoefficients);
        }

        public override string ToString()
        {
            var stringToReturn = new StringBuilder();
            for (var i = 0; i <= _power; i++)
            {
                if (Math.Abs(_coefficients[i]) > 0)
                {
                    stringToReturn.Append($"{_coefficients[i]} * x^{i} + ");
                }
            }

            stringToReturn.Remove(stringToReturn.Length - 2, 2);
            return stringToReturn.ToString();
        }

        public double CalculateValueOfArgument(double argument)
        {
            double sum = 0;
            for (var i = 0; i <= _power; i++)
            {
                sum += _coefficients[i] * Math.Pow(argument, i);
            }

            return sum;
        }

        public void Print()
        {
            Console.WriteLine(this);
        }

        private static Polynomial PerformOperation(Polynomial morePowerPolynomial, Polynomial lessPowerPolynomial, Func<double, double, double> function)
        {
            var resultArrayOfCoefficients = new double[morePowerPolynomial._power + 1];
            for (var i = 0; i <= lessPowerPolynomial._power; i++)
            {
                resultArrayOfCoefficients[i] = function(morePowerPolynomial._coefficients[i], lessPowerPolynomial._coefficients[i]);
            }

            for (var i = lessPowerPolynomial._power + 1; i <= morePowerPolynomial._power; i++)
            {
                resultArrayOfCoefficients[i] = morePowerPolynomial._coefficients[i];
            }

            return new Polynomial(morePowerPolynomial._power, resultArrayOfCoefficients);
        }

        private static Polynomial ChangeSignOfCoefficients(Polynomial polynomial)
        {
            var resultArrayOfCoefficients = new double[polynomial._power + 1];
            for (var i = 0; i <= polynomial._power; i++)
            {
                resultArrayOfCoefficients[i] = polynomial._coefficients[i] * (-1);
            }

            return new Polynomial(polynomial._power, resultArrayOfCoefficients);
        }
    }
}
